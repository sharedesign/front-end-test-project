Front-end Test Project
======================

## Project brief
Convert the following designs to HTML/CSS/JS.

### Thumbnails
![ShareDesign Front-end Test Thumbnails](sd-front-end-test-thumbs.jpg)

### Overlay
![ShareDesign Front-end Test Overlay](sd-front-end-test-overlay.jpg)

## Requirements
1. Create project using [Blendid](https://github.com/vigetlabs/blendid)
2. Use HTML5 / CSS
3. Make it responsive using your best judgement.
4. Create a simple custom overlay for photos - use only vanilla JavaScript, do not use jQuery or any other external libraries for it.
5. Create some hover effect for the image thumbnails.
6. Make the page the smallest possible size - ensure that images are properly optimized, resources minified, etc.
7. *Optional bonus task 1:* Use [responsive images](https://jakearchibald.com/2015/anatomy-of-responsive-images/)
8. *Optional bonus task 2:* Add basic routing – make browser's back button work properly; open overlay based on URL.

## Download PSD
[sd-front-end-test.zip](sd-front-end-test.zip?raw=true) (12,6MB)

- The font used in the design is Open Sans
- The following images are used in the design:
  - https://pixabay.com/en/rotterdam-water-skyline-bridge-2352011/
  - https://pixabay.com/en/holland-netherlands-rotterdam-1637467/
  - https://pixabay.com/en/rotterdam-netherlands-building-2428755/
  - https://pixabay.com/en/rotterdam-lock-pink-love-bridge-672983/


## Supported browsers
Ensure that the elements work and display correctly in the following browsers:

- Firefox (latest version)
- Google Chrome (latest version)
- Microsoft Edge
- Internet Explorer 11

## Coding Standards
When working on the project use consistent coding style. Try to follow guidelines like [Code Guide](http://codeguide.co/) or [CSS Guidelines](http://cssguidelin.es/).

## Project Deadline
Take your time but try to deliver it within 2 weeks time. If we don't see any activity in your test repository after 2 weeks (at least initial commits), we will automatically withdraw your application.

## Quality Assurance

What you need to do to get high QA score? Simply answer **Yes** to all these questions:

### General

- Are all requirements set above met?
- Can the project be built using `yarn run blendid -- build`?
- Is the page working without any JS errors?

### Precision

- Is reasonable precision achieved?

### Browser check

- Does page display and work correctly in supported browsers?

### Valid HTML

- Is the page valid?

### Semantic Markup

- Are the correct tags being used?

### Coding Standards

- Is the page using a consistent HTML coding style?
- Is the page using a consistent CSS coding style?
- Is the page using a consistent JS coding style?

### Optimization

- Are image files sufficiently compressed?
- Is CSS and JS concatenated and minified?

### Accessibility

- Are proper ALT attributes for images provided?
- Are ARIA attributes properly used?
- Is proper heading structure in place?
